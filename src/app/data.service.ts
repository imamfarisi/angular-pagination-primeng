import { Injectable } from "@angular/core";
import { map, Observable, of } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class DataService {
    data = {
        "total": 7,
        "data": [
            {
                "id": 11,
                "title": "abc"
            },
            {
                "id": 12,
                "title": "cde"
            },
            {
                "id": 13,
                "title": "efg"
            },
            {
                "id": 14,
                "title": "hij"
            },
            {
                "id": 25,
                "title": "klm"
            },
            {
                "id": 26,
                "title": "nop"
            },
            {
                "id": 27,
                "title": "pqr"
            }
        ]
    }

    getAll(startPage: number, maxPage: number, query?: string): Observable<any> {
        //call your real api backend here using http client
        return of(this.data)
            .pipe(
                map(res => {
                    let resAny: any = { ...res }
                    if (query) {
                        resAny.data = resAny.data.filter((d: any) => d.id.toString().includes(query) || d.title.includes(query))
                        resAny.total = resAny.data.length;
                    }
                    resAny.data = resAny.data.slice(startPage, startPage + maxPage)
                    return resAny
                }),
            );
    }

    deletePost(id: number): Observable<any> {
        //call your real api backend here using http client
        const finalData = this.data.data.filter(d => d.id != id)
        this.data = { total: this.data.total - 1, data: finalData }
        return of("success")
    }

}