import { Component, OnDestroy } from '@angular/core';
import { LazyLoadEvent } from 'primeng/api/lazyloadevent';
import { Subscription } from 'rxjs';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  data: any[] = []

  startPage: number = 0
  maxPage: number = 5
  totalData: number = 0
  query?: string
  loading: boolean = true

  getAllSubs?: Subscription
  deleteSubs?: Subscription

  constructor(private dataService: DataService) { }

  loadData(event: LazyLoadEvent) {
    console.log(event)
    this.getData(event.first, event.rows, event.globalFilter)
  }

  getData(startPage: number = this.startPage, maxPage: number = this.maxPage, query?: string): void {
    this.loading = true;
    this.startPage = startPage
    this.maxPage = maxPage
    this.query = query

    this.getAllSubs = this.dataService.getAll(startPage, maxPage, query).subscribe(
      result => {
        const resultData: any = result
        this.data = resultData.data
        this.loading = false
        this.totalData = resultData.total
        console.log(resultData)
      },
    )
  }

  deletePost(id: number): void {
    this.deleteSubs = this.dataService.deletePost(id).subscribe(_ => this.getData(this.startPage, this.maxPage, this.query))
  }

  ngOnDestroy(): void {
    this.getAllSubs?.unsubscribe()
    this.deleteSubs?.unsubscribe()
  }
}